= 3710 Recertifications (Renewals)
:chapter-number: 3700
:effective-date: January 2025
:mt: MT-80
:policy-number: 3710
:policy-title: Recertifications (Renewals)
:previous-policy-number: MT-79

include::partial$policy-header.adoc[]

== Requirements

A review of the Assistance Unit's (AU) circumstances is required to continue Supplemental Nutrition Assistance Program (SNAP) benefits.
The recertification (renewal) process includes filing an application, having an interview (if required), and providing required information.

Notification is sent to the AU regarding its eligibility status.

== Basic Considerations

A renewal application is an application to continue SNAP benefits.
All points of eligibility are reestablished, and all discrepancies are resolved.
Benefits [.underline]*are not* continued beyond the end of the certification period without reestablishing the AU's eligibility.

An AU must complete the renewal process in the last month of the current certification period to receive uninterrupted benefits. The date of the renewal application is the date the form is received by the agency.

=== Types of Recertifications (Renewals)

Renewals are identified and processed as follows:

* Timely
* Untimely
* Late

=== Timely Recertification (Renewals)

A *timely renewal* is an application for continued benefits submitted by the AU between the 1^st^ and 15^th^ day of the last month of the certification period.
Eligible AUs are entitled to receive uninterrupted benefits (i.e., benefits are available by their normal issuance cycle).

NOTE: A renewal interview completed in the month prior to the last month of the certification period is processed as a timely renewal.


=== Untimely Recertifications (Renewals)

An *untimely renewal* is an application for continued benefits submitted by the AU between the 16^th^ and last day of the month of the current certification period.

AUs filing untimely renewals lose the right to receive uninterrupted benefits.

=== Late Recertifications (Renewals)

A late renewal is an application filed within 30 days following the expired certification period if the AU [.underline]#fails to file a renewal application# in the last month of the certification period.

Forms received in the month following the last month of the certification period are treated as *late renewals* and *benefits are prorated* from the date the form is received. Refer to <<chart-3710-1,Chart 3710.1, Processing SNAP Recertifications (Renewals)>>, in this section.

=== Reported Changes

Changes reported during the renewal are effective the first month of the new certification period.

=== Expedited Changes

Expedited changes reported in the month preceding the last month of the certification period are processed according to the Interim Changes policy in Section 3715 of the manual. Refer to xref:3715.adoc[]

=== Periodic Reporting

Periodic reporting is required for households that are certified for longer than 6 months.
Periodic reporting is currently waived for Senior SNAP households, unless the household becomes ineligible for Senior SNAP, but remains eligible for SNAP during their 36-month certification period.

NOTE: *Periodic reporting is not a recertification.*

See xref:3730.adoc[] for Periodic Reporting policy.

=== Expedited Services

AUs that reapply after an expired certification period or within the 30 days following the last month of the certification period may be eligible for expedited services.
Screen the renewal application to determine potential eligibility for expedited services.

NOTE: AUs reapplying before the end of their current certification period are not eligible for expedited services.

=== Standard of Promptness (SOP)

For timely renewals, approvals and denials must be completed by the end of the last month of the current certification period.
Eligible AUs must receive notification of their eligibility and notification of receipt of their benefits by the next issuance cycle.
Ineligible AUs must receive notification of their denial by the end of the last month of the current certification period.

An untimely renewal must be approved in sufficient time for the AU to receive benefits by the 30^th^ day from the date of the renewal application.
Denials must be completed by the 30^th^ day from the date of the renewal application.
Refer to <<chart-3710-1,Chart 3710.1, Processing SNAP Recertifications (Renewals)>>, in this section.

=== Recertification (Renewal) Notices

A renewal notice, either system-generated or manual, is used to notify the AU that the certification period is about to end.
Copies of manual notices are uploaded in DIS (Document Imaging System).
Notification that a renewal is due and is about to expire must contain the following information:

* the date on which the certification period ends
* the date by which the AU must file an application for renewal to receive uninterrupted benefits
* the consequences of failing to file an application for renewal in a timely manner
* the right to receive a renewal application and have it accepted if it contains a name, address, and signature
* the right to file a request for an application for renewal by mail or in person
* that the AU must complete an interview if an interview is required
* that it is the AU's responsibility to reschedule a missed interview
* that the AU must complete the renewal process and provide the required verification
* the consequences of failing to comply with the renewal process
* the address of the county office where the application can be filed
* the name and telephone number of the eligibility worker
* that AUs with SSI only members may file an application for renewal at the SSA office
* the right to request a fair hearing

[#interview-requirements]
=== Interviewing Requirements

If the AU fails to attend the initial interview but contacts the agency to request another interview, then the assigned eligibility worker (EW) must reschedule the interview.

An interview with the head of the AU, an authorized representative, or another responsible AU member, is required at least once every 12 months or every 4 months for ABAWD households. Households certified for 24 months are required to be interviewed at every recertification. The interview should be a telephone interview unless the AU requests a face-to-face interview. For policy on Senior SNAP interviews, refer to xref:3725.adoc[]

When the AU reports difficulty completing a scheduled interview, explore all interview options.
To the extent practical, the interview must be scheduled to accommodate the needs of AUs with special circumstances, such as the following:

* Working AUs
* AUs with elderly and/or disabled members
* AUs with ill members
* AUs in rural areas or who have transportation issues
* AUs in prolonged severe weather
* AUs residing on reservations
* AUs with adult members who do not speak English

== Screening and Referral to SNAP Employment and Training (E&T)

The eligibility worker is required to screen all AU members who meet the following criteria during the interview to determine if a referral is appropriate to the SNAP E&T Program, also known as SNAP Works:

* are at least 16 or 17 years of age [.underline]*and* the head of household
* are at least 18 years or older
* are not receiving TANF benefits
* are fit for employment

For each AU member who meets the criteria, the SNAP Screening and Referral Tool page in Gateway (Form 864) must be completed even if they are unavailable during the interview. If Gateway is down and screening cannot be completed in the system, a manual form 864 must be completed and uploaded.

The Primary Individual (PI) can respond for other AU members that need to be screened if they wish to do so.

If the PI states during the interview they don't know if the other AU members would want to be referred the eligibility worker should make an attempt to screen all AU members to determine appropriateness to be referred to SNAP Works.

If the PI provides screening information for other AU members but is unsure if they are ready to volunteer to be referred, then the eligibility worker should document accordingly.

If the PI states that they do not feel comfortable answering for other AU members regarding a referral, eligibility workers should try to complete the screening and referral process and document accordingly.

If the screening and referral process is not completed at application or recertification for absent AU members, then that should also be documented.
In that case, those AU members would need to go through the screening and referral process at the next application or recertification, even if the next recertification is an alternate renewal.

=== Recertification (Renewal) Application Forms

The following forms must be completed and/or signed by the AU or the household's authorized representative, where applicable, when processing a SNAP renewal:

* Form 297, Application for Benefits and Form 297A, Rights and Responsibilities, if Form 297 is used
* Form 508, SNAP/Medicaid/TANF Renewal Form
* Form 859, SNAP Consolidated Work Requirements Notice (if work registrants are in the AU) This must be mailed manually if not sent out by Gateway.
* Form 846, Change Report Form, if requested by the AU

=== Electronic Signatures

Electronic signatures utilizing Adobe Signature or other E-Signature software will be acceptable for all SNAP forms. Examples of electronic signatures are the use of a Personal Identification Number (PIN), a computer password, clicking on an “I accept these conditions” button on a screen, or clicking on a “Submit” button on a screen. Adobe Signature, DocuSign, and Dropbox Sign are common software that offer E-Signature.
A completed application consists of a signed application submitted with a name and address.
A typed name on the signature line of a paper application is not acceptable.

NOTE: If an authorized representative applies on behalf of a customer, the application must be signed by either a responsible member of the household or the household's authorized representative.

== Procedures

=== Standard Renewals

A standard renewal is a review of eligibility that [.underline]*requires* either a telephone or face-to-face interview.
Refer to <<interview-requirements, Interviewing Requirements>> in this section.

NOTE: All AUs containing an ABAWD must receive a standard renewal.
This is to determine the correct work registration status of the recipient regardless of the county status.

Follow the steps below to complete a standard renewal.

[horizontal,labelwidth=10]
*Step 1:* Mail the AU a renewal notice in the month preceding the last month of the certification period in sufficient time so that the AU may submit a renewal application by the 15th day of the last month of the certification period.

*Step 2:* Conduct an interview with the appropriate individual.
A Notice of Missed Interview (NOMI) is sent if the interview appointment is missed.

*Step 3:* Review the last initial or renewal application and all points of eligibility.
Obtain verification when required.
Refer to xref:3035.adoc#chart-3035-1[Chart 3035.1], in Section 3035, Verification, for the Summary Verification Chart for Recertification.

*Step 4:* Review all clearinghouse interfaces (DOL wage history, SDX/BENDEX, CSS, Truv and SteadyIQ (if available), DOC, NAC, eDRS, etc).

*Step 5:* Document the information obtained and verified during the renewal process in the Gateway system.

*Step 6:* Update the Gateway system to reflect changes and to indicate that the renewal is complete, assigning a new certification period.

*Step 7:* Refer to Section 3105, Application Processing, xref:3105.adoc#chart-3105-1[Chart 3105.1, Assigning Certification Periods].

If the form is not returned or all required verification is not received by the end of the appropriate SOP, close the case.

=== Alternate Renewals

Due to policy changes surrounding the implementation of Periodic Reports, alternate renewals are no longer applicable for regular SNAP cases authorized on or after November 18, 2023.

*Use the information in the chart below to process SNAP renewals.*

[#chart-3710-1]
.CHART 3710.1 - PROCESSING SNAP RECERTIFICATIONS (RENEWALS)
|===
^| IF THE ^| THEN

| AU files a renewal application on or before the 15^th^ day of the last month of the current certification period *(timely renewal)*

*AND*

has met all renewal requirements
| Process the renewal application by the end of the last month of the current certification period.
If the end of the current certification period falls on a weekend or holiday, process the renewal application by the last workday prior to the end of the current certification period.

| AU files a renewal application on or before the 15^th^ day of the last month of the current certification period *(timely renewal)*

*AND*

[.underline]#has not completed an interview# or has made no contact with the agency by the last day of the current certification period
a| Deny the renewal application by the end of the last month of the current certification period.
If the end of the current certification period falls on a weekend or holiday, deny the renewal application by the last workday prior to the end of the current certification period.

NOTE: Send a NOMI if the scheduled interview has been missed.

| AU files a renewal application on or before the 15^th^ day of the last month of the current certification period *(timely renewal)*

*AND*

[.underline]#fails to return required verification# by the requested deadline (This does not include allowable deductions.  See scenario further down for how to process deductions.)
| Deny the renewal application after the verification deadline, but no later than the end of the last month of the current certification period.
If the end of the current certification period falls on a weekend or holiday, deny the renewal application by the last workday prior to the end of the current certification period.

If verification is submitted before the last day of the certification period, re-open the renewal application and provide a full month of benefits beginning with the first month of the new certification period.

| AU files a renewal application between the 16^th^ and last day of the month of the current certification period *(untimely renewal)*

*AND*

has met all renewal requirements
| Process the renewal application by the 29^th^ day following the date of the renewal application.

| AU files a renewal application between the 16^th^ and last day of the month of the current certification period *(untimely renewal)*

*AND*

[.underline]#has not completed an interview# or made any contact with the agency by the 30th day following the renewal application date
a| Deny the renewal application on the 30^th^ day following the date of application.
If the 30^th^ day falls on a weekend or holiday, the application should be denied on the next business day.

NOTE: Send a NOMI if the scheduled interview is missed.
A new application is required if the AU makes another request to participate in the program.

| AU files a renewal application between the 16^th^ and last day of the month of the current certification period *(untimely renewal)*

*AND*

[.underline]#fails to return required verification# by the requested deadline (This does not include allowable deductions.  See scenario further down for how to process deductions.)
| Deny the renewal application on the day after the verification deadline but [.underline]#no later than# the 30^th^ day following the date of application.
If the 30^th^ day falls on a weekend or holiday, the application should be denied on the next business day.

| AU files a timely or untimely application and the application was denied for failure to return verification, and/or the SOP has expired

*BUT*

Verification was returned within 30 days following the last month of the current certification period
| Prorate benefits from:

The date the verification is received, and all renewal requirements are met.
Process the case within 5 workdays after receipt of the verification.

| AU [.underline]#fails to file a renewal application# in the last month of the current certification period but files an application within 30 days following the last month of the current certification period (late renewal)
| Screen the application to determine if the AU is eligible for expedited services.
If eligible, provide benefits by the appropriate SOP based on expedited or non-expedited processing policy.
Use the renewal verification requirements to process the case.

Prorate benefits from the date of application.

| AU fails to provide verification for an allowable deduction, the case is approved without the deduction

*AND*

Verification is received prior to the first month of the new certification period
| Issue a restoration retroactive to the first month of the new certification period.

| AU fails to provide verification of an allowable deduction; the case is approved without the deduction

*AND*

Verification is provided on or after the first day of the first month of the new certification period
| Update the case as a reported change effective the month following the month the verification is received.

| Renewal is severely delayed and has not been processed in more than 12 months (one year) because of the agency's failure to take action
| Authorize benefits retroactive to 12 months (one year) only.

If benefits were lost for more than 12 months, notify the AU of their entitlement for the 12-month period, the benefit amount to be restored, any offset of benefits, and how benefits will be restored.
Refer to Section 3810, Issuance, for policy regarding underpayment and overpayment of benefits.
|===

NOTE: The agency must give assistance units (AU) at least 10 calendar days to provide any required verification.
Refer to xref:3035.adoc[].
