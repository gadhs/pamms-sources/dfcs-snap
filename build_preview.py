import sys
import os
import shutil

def write_to_yaml_file(path, content, policy_section):
    # Write content to a yaml file
    try:
        with open(path, "w") as file:
            file.write(content)
        print(f"File '{content}' created successfully for '{policy_section}'.")
    except Exception as e:
        print(f"An error occurred: {e}")


# Assign arguments to variables
policy_section = sys.argv[1]
directory_path = f"{sys.argv[2]}/home/ROOT/pages"
commit_ref = sys.argv[3]

# create path for output
antora_preview_path =  f"{sys.argv[2]}/home/antora.yml"
antora_playbook_path = "antora-playbook.yml"

# yaml template for antora.yml
antora_preview_content = f"""
name: ROOT
title: Home
version: ~
"""

# yaml template for playbook preview
antora_playbook_content = f"""
site:
  title: PAMMS - {policy_section.capitalize()} Preview Page
  url: https://gadhs.gitlab.io/pamms-sources/dfcs-{policy_section}
  start_page: dfcs-{policy_section}:{policy_section}:index.adoc
urls:
  latest_version_segment: latest
  redirect_facility: gitlab
content:
  edit_url: '{{web_url}}/edit/{commit_ref}/{{path}}'
  sources:
    - url: .
      start_path: home
    - url: .
    - url: ./workspace/dfcs-common

asciidoc:
  attributes:
    experimental: ''
    hide-uri-scheme: '@'
    idprefix: ''
    idseparator: '-'
    page-pagination: ''
    table-caption: false
antora:
  extensions:
    - require: '@antora/lunr-extension'
ui:
  bundle:
    url: https://gitlab.com/gadhs/pamms-sources/pamms-ui/-/jobs/artifacts/gadhs-main/raw/build/ui-bundle.zip?job=bundle-stable
    snapshot: true
runtime:
  fetch: true
  cache_dir: .cache/antora
  log:
    level: warn
output:
  dir: public
"""

def main():

    # Check for arguments
    if len(sys.argv) < 2:
        print("Usage: python create_file.py <policy_section> <directory_path> <commit_ref_name>")
        sys.exit(1)

    # make directory path
    os.makedirs(directory_path, exist_ok=True)

    # create yaml for antora_preview
    write_to_yaml_file(antora_preview_path, antora_preview_content, policy_section)

    # create yaml for antora playbook
    write_to_yaml_file(antora_playbook_path, antora_playbook_content, policy_section)

    shutil.copy(f"modules/{policy_section}/pages/pamms.adoc", f"{directory_path}/index.adoc")

if __name__ == "__main__":
    main()